// create a location/storage where the friends list will be stored

let friendsList = [];
console.log(friendsList)
let res = document.getElementById("responseMessage")
let length = friendsList.length;
// Mutator functions
	// create a function to add new friend

function addNewFriend() {
		//get the data from the user
		let friendName = document.getElementById("friendName").value;
	//validate the information by creating a structure that will make sure that the input is NOT empty
	if (friendName !== "") {
		//proceed with the function
		friendsList.push(friendName);
		length = friendsList.length
	} else {
		res.innerHTML = '<h5 class="mb-2 mt-5 text-danger">Invalid value</h5>';
	}
}
// Iteration functions

//create function to check how many friends

function viewListLength() {
	// we will assess the length of the array and inform the user how many elements are there in the array
	if (length === 0) {
		//inform the user
		res.innerHTML = "You currently have zero friends"
	} else if (length > 0) {
		res.innerHTML = "You currently have " + length + " friends"		
	}
}