// basic array structure

let bootcamp = 
["Learn HTML",
 "Use CSS",
 "Understand JS",
 "Maintain MongoDB",
 "Create component using React"
]

// access elements inside an Array

console.log(bootcamp);

console.log(bootcamp[0]); //Learn HTML
console.log(bootcamp[4]); //Create component using React
console.log(bootcamp[9]); //undefined


//getting the length of an array structure
console.log(bootcamp.length)

/*if (bootcamp.length > 5) {
	console.log("this is the length of the array")
}*/

//how to access the last element of an array
console.log("Batch 145")

// 2 types of array
// mutators and iterator

// mutators - seek to MODIFY the contents of an array

let bootcampTasks = [];

//push() - will add an element at the end of an array
bootcampTasks.push("Learn Javascript", "Building a server using Node", "Utiziling Express to build a server");


//pop() - removes last the element of the array and be can be repackaged inside a new variable.
let removedItemUsingPop = bootcampTasks.pop();
console.log(removedItemUsingPop);


//unshift() - add one or more element at the beginning of the array.
bootcampTasks.unshift("Understand the concept of REST API", "How to use Postman", "Learn how to use MongoDB");


//shift() - removes the first element of the array.
let removedItemUsingShift = bootcampTasks.shift();
console.log(removedItemUsingShift);


// splice() - we can extract and insert new values to the container.
// syntax arrayName.splice(startingPosition, numberOfElementsToRemove, elementsToBeAdded)

bootcampTasks.splice(0, 1, "Learn Wireframing", "Learn React")
console.log(bootcampTasks);

//sort() - arranges the array alphanumerically

let library = ["Pride and Prejudice", "The Alchemist", "Diary of a pulubi", "Beauty and the Beast"];
let series = [2, 8 ,1, 8, 17, 12, 1];
let asd = ["aac","abc", "aaa", "aab" ]
asd.sort()
console.log(asd)

//reverse() - reverses the order of the elements in an array

series.reverse()
console.log(series)

//ACCESSORS

// indexOf() - find/identify the index number of a given element where it is FIRST FOUND

let countries = ["US", "PH", "NZ", "CAN", "SG", "NZ"]
let indexCount = countries.indexOf("NZ")
console.log(indexCount)

// lastIndexOf() find/identify the index of a given element where it is LAST FOUND
let lastFound = countries.lastIndexOf("NZ")
console.log(lastFound)


// ITERATORS - aim to EVALUATE each element in an array
// 
// forEach() - the function inside forEach is called an anonymous function. these are functions that are defined and executed once only.
// syntax: array.forEach( function(){what you want to do to each element})
// were going to pass a parameter inside the function that will describe each single inside the array
bootcamp.forEach( function(task){
    console.log(task)
})


//map()
let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
// pass a parameter inside the function to identify each element inside the array.
numbers.map(function(num){
    // this will tell the function to do for each element.
    // only return the values in the series that will be the result of each number multiplied by itself
    console.log(num * num)
});

// every() - will check all the elements that will pass a certain condition.
let checker = numbers.every(function(num) {
    // we can specify the return.
    return(num <= 5);
});
console.log("Did all the numbers passed? " + checker)

//some() - atleast 1 should pass the condition
let isGreaterThan7 = numbers.some( function(num) {
    return (num >= 7);
});
console.log("Did some of them passed? "+isGreaterThan7);

// filter() - creates a new array that contains elements which passed a certain condition

let money = [1200, 2400, 6300, 990, 256];
let newMoney = money.filter(function(arep) {
    return(arep > 2000);
});

console.log(money);
console.log(newMoney);

//reduce() - evaluates elements from left to right and returns a single value.
money.reduce(function(initialEl, nextEl) {
    return initialEl + nextEl;
});

let currency =  [ null, "PHP", "WON", "NZD"]

let reducedOutput = currency.reduce(function(left, right) {
    return left + right
})
console.log(reducedOutput)