let students = []; 
let sectionedStudents = []; 

function addStudent(name) {
  //call out the array and use the push() to add a new element inside the container.
  students.push(name); 
  //display a return in the console
  console.log(name + ' has been added to the list of students');
}

addStudent('Zyrus');
addStudent('Marie');
addStudent('John'); 
console.log(students); 

function countStudents() {
  //display the number of elements inside the array with the prescribed message. 
  console.log('This Class has a total of '+ students.length +' enrolled students');
} 

//invocation 
countStudents(); 


function printStudents() {
   //sort the elements inside the array in alphanumeric order.
   students = students.sort();
   //console.log(students); 
   //we want to display each element in the console individually.
   students.forEach(function(student) {
   	 console.log(student); 
   })
}

printStudents(); 

function findStudent(keyword) {
	// describe each element inside the array individually.
	let matches = students.filter(function(student) {
		return student.toLowerCase().includes(keyword.toLowerCase())
	})
if (matches.length === 1) {
	//if there is a match/es
	console.log(matches[0] + " is enrolled")
} else if (matches.length > 1) {
	console.log("Multiple students matches this keyword")
} else {
	//if no result/s was found
	console.log("No student matches this keyword")
}
}



function addSection(section) {
  //place each student inside the students array inside an array called sectionedStudents and add a section for each element inside the array.
 sectionedStudents = students.map(function(student) {
  	return student + " is part of section: " + section;
  })
 console.log(sectionedStudents)
}
addSection("5")
//map() - will create a new array populated with the elements that passes a condition on a given function/argument

function removeStudent(name) {
	let result = students.indexOf(name);
	if (result >= 0) {
		//remove the element using splice()
		let removed = students.splice(result, 1)
		console.log(removed + " was removed")
	} else {
		console.log("no student was removed")
	}
}
removeStudent("Zyrus")